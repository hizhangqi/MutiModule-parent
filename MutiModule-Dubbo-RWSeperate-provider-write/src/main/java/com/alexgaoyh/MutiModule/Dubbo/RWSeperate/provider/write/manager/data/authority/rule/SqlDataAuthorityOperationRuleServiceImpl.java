package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.write.manager.data.authority.rule;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.rule.write.ISqlDataAuthorityOperationRuleWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModel;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.model.SqlDataAuthorityOperationModelMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule.SqlDataAuthorityOperationRule;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule.SqlDataAuthorityOperationRuleMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationView;

@Service(value = "sqlDataAuthorityOperationRuleService")
public class SqlDataAuthorityOperationRuleServiceImpl implements ISqlDataAuthorityOperationRuleWriteService{
	
	@Resource(name = "sqlDataAuthorityOperationRuleMapper")
	private SqlDataAuthorityOperationRuleMapper mapper;
	
	@Resource(name = "sqlDataAuthorityOperationModelMapper")
	private SqlDataAuthorityOperationModelMapper modelMapper;

	@Override
	public int deleteByPrimaryKey(String id) {
		return mapper.deleteByPrimaryKey(id);
	}

	@Override
	public int insert(SqlDataAuthorityOperationRule record) {
		return mapper.insert(record);
	}

	@Override
	public int insertSelective(SqlDataAuthorityOperationRule record) {
		return mapper.insertSelective(record);
	}

	@Override
	public int updateByPrimaryKeySelective(SqlDataAuthorityOperationRule record) {
		return mapper.updateByPrimaryKeySelective(record);
	}

	@Override
	public int updateByPrimaryKey(SqlDataAuthorityOperationRule record) {
		return mapper.updateByPrimaryKey(record);
	}

	@Override
	public int insertSqlDataAuthorityOperationView(SqlDataAuthorityOperationView entity) {
		SqlDataAuthorityOperationRule _rule = (SqlDataAuthorityOperationRule)entity;
		mapper.insert(_rule);
		List<SqlDataAuthorityOperationModel> _modelList = entity.getItems();
		if(_modelList != null && _modelList.size() > 0) {
			for (SqlDataAuthorityOperationModel sqlDataAuthorityOperationModel : _modelList) {
				modelMapper.insert(sqlDataAuthorityOperationModel);
			}
		}
		return _modelList.size();
	}
	
}
