package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.demo.oneWithMany.course;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.course.read.IOneWithManyCourseReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course.OneWithManyCourse;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course.OneWithManyCourseMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.view.CourseStudentView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;

@Service(value = "oneWithManyCourseService")
public class OneWithManyCourseServiceImpl implements IOneWithManyCourseReadService{
	
	@Resource(name = "oneWithManyCourseMapper")
	private OneWithManyCourseMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<OneWithManyCourse> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public OneWithManyCourse selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public MyPageView<OneWithManyCourse> generateMyPageViewVO(Map<Object, Object> map) {
		int _totalCount = mapper.selectCountByMap(map);
		
		List<OneWithManyCourse> _list = mapper.selectListByMap(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<OneWithManyCourse> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
	}

	@Override
	public List<CourseStudentView> selectCourseWithStudentByExample(Map<Object, Object> map) {
		return mapper.selectCourseWithStudentByExample(map);
	}

	@Override
	public MyPageView<CourseStudentView> generateMyPageViewVOWithStudent(Map<Object, Object> map) {
		int _totalCount = mapper.selectCountByMap(map);
		
		List<CourseStudentView> _list = mapper.selectCourseWithStudentByExample(map);
		
		int recordPerPage = 10;
		int currentPage = 1;
		if(map.get("page") != null) {
			Object _pageObj = map.get("page");
			if(_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page)_pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<CourseStudentView> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage, _totalCount, _list);
		
		return pageView;
	}
	
	


}
