package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.dict.dictionary.item;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.myPage.util.MyPageViewUtilss;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.dict.dictionary.item.read.IDictDictionaryItemReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItem;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.item.DictDictionaryItemMapper;

@Service(value = "dictDictionaryItemService")
public class DictDictionaryItemServiceImpl implements IDictDictionaryItemReadService {

	@Resource(name = "dictDictionaryItemMapper")
	private DictDictionaryItemMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<DictDictionaryItem> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

	@Override
	public DictDictionaryItem selectByPrimaryKey(String id) {
		return mapper.selectByPrimaryKey(id);
	}

	@Override
	public MyPageView<DictDictionaryItem> generateMyPageViewVO(Map<Object, Object> map) {

		int _totalCount = mapper.selectCountByMap(map);

		List<DictDictionaryItem> _list = mapper.selectListByMap(map);

		int recordPerPage = 10;
		int currentPage = 1;
		if (map.get("page") != null) {
			Object _pageObj = map.get("page");
			if (_pageObj instanceof com.MutiModule.common.vo.mybatis.pagination.Page) {
				com.MutiModule.common.vo.mybatis.pagination.Page _page = (com.MutiModule.common.vo.mybatis.pagination.Page) _pageObj;
				currentPage = _page.getPageNo();
				recordPerPage = _page.getLength();
			}
		}
		MyPageView<DictDictionaryItem> pageView = MyPageViewUtilss.generaterMyPageView(recordPerPage, currentPage,
				_totalCount, _list);

		return pageView;

	}

}
