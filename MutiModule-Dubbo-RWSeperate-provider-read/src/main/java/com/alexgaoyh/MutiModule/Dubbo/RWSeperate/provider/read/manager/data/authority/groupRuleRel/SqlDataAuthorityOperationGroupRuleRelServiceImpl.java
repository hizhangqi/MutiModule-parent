package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.provider.read.manager.data.authority.groupRuleRel;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.groupRuleRel.read.ISqlDataAuthorityOperationGroupRuleRelReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.groupRuleRel.SqlDataAuthorityOperationGroupRuleRelKey;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.groupRuleRel.SqlDataAuthorityOperationGroupRuleRelMapper;

@Service(value = "sqlDataAuthorityOperationGroupRuleRelService")
public class SqlDataAuthorityOperationGroupRuleRelServiceImpl implements ISqlDataAuthorityOperationGroupRuleRelReadService{
	
	@Resource(name = "sqlDataAuthorityOperationGroupRuleRelMapper")
	private SqlDataAuthorityOperationGroupRuleRelMapper mapper;

	@Override
	public int selectCountByMap(Map<Object, Object> map) {
		return mapper.selectCountByMap(map);
	}

	@Override
	public List<SqlDataAuthorityOperationGroupRuleRelKey> selectListByMap(Map<Object, Object> map) {
		return mapper.selectListByMap(map);
	}

}
