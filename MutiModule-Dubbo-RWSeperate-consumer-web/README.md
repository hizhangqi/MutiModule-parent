#20151112
	Dubbo 服务消费者，引入接口部分，进行web展示层的数据表现；
	
#20151120
	JSTL实现递归展示树型结构数据！
		TreeNodeJSTLController.java 封装 TreeNode 格式的数据类型，父子关系； 最主要的是jsp页面的递归调用(因为未知树的层级是多少，JSTL中又不支持函数递归,解决方案：使用文件包括的形式的解决)
			两个页面路径分别为：    \treeNodeJSTLEach\_r.jsp  \treeNodeJSTLEach\index.jsp
			
#20151125
	inspinia+ admin templete 框架使用， 整合登陆和左侧菜单栏（菜单栏最多支持三层结构）
	
#20151126
	增加sitemesh 装饰器功能；org.sitemesh。sitemesh   将通用的部分页面展现抽离出来，后期只关注业务相关的展示页面即可；
		下一步增加  AOP 相关，将左侧菜单栏部分的数据层展现抽离出来， 并且后期权限校验相关也可以放置到这个AOP 里面
			下一步增加 BaseController 类，抽离通用的方法，尽量做到只需继承此方法，即可完成简单的CRUD 相关功能；
			
	增加对 com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController 基类的AOP相关控制，用来在此处增加左侧菜单栏的数据保存控制
	
	Controller层注解部分    @RequestMapping(value="manager/*")
		在 	manager/* 路径下的话，将走sitemesh 装饰器；
		
	Controller层部分使用   extends BaseController 的话，将走AOP 的配置：(左侧菜单栏的部分数据封装显示)
		com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.aop.LeftMenuDataAop 
		
#20151130
	列表页功能完成；仅完成列表页数据展现相关；
	
#20151201
	Dubbo-RWSeperate-consumer-web模块：整合inspinia+模板分页功能	
		实现 分页栏部分/每页多少条数据  事件相应功能，条件筛选功能，数据回显相关；
		
#20151229
	Dubbo-RWSeperate-consumer-web模块: 后台用户管理CRUD功能实现；
		manager.jsp页面引入common.jsp 文件
			优化CRUD部分的页面部分，下一步优化controller部分的代码；
			
#2016010
	comsumer-web模块，list列表展示页面增加如下js css 引入；解决小屏幕窗口自适应问题；兼容手机端相应
	响应式布局，解决列表页面自适应问题；
		<!-- Data Tables -->
    	<link href="${staticUrl_ }/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
    	<link href="${staticUrl_ }/css/plugins/dataTables/dataTables.responsive.css" rel="stylesheet">
		<!-- Data Tables -->
		<script src="${staticUrl_ }/js/plugins/dataTables/jquery.dataTables.js"></script>
    	<script src="${staticUrl_ }/js/plugins/dataTables/dataTables.bootstrap.js"></script>
    	<script src="${staticUrl_ }/js/plugins/dataTables/dataTables.responsive.js"></script>
    	
#20160123
	增加 jquery-cookie.js 文件，用来前端编辑cookie值；
		本例增加jquery-cookie.js 文件，用来记录选中左侧菜单栏的哪一个，刷新页面后重新打开此菜单栏部分；
		
#20160124
	增加jetty启动的功能部分；同时增加json2.js 插件，用来解决前端json解析功能；
	
#20160125
	consumer-web模块： sysmanResource 功能编写；
	
#20160226
	comsumer-web模块： 新增 BaseEmptyController 类文件，解决 AOP(LeftMenuDataAop) 时部分方法不需依赖 BaseController 的问题：
		public class BaseController<E extends Serializable> extends BaseEmptyController
		
	consumer-web模块：  增加无此权限的情况，直接不显示左侧菜单栏；

#20160229
	consumer-web模块： 修复bug: sys_sysmanresource表中 parent_ids 字段的处理，修复字段保存不准确的情况；
						     修复bug:	 jquery.metisMenu.js 文件，控制菜单回显展开；
						     	$('#li-' + leftMenuId).addClass("active");
						    	$('#li-nav-second-' + leftMenuId).addClass("active");
						    	$('#ul-nav-second-' + leftMenuId).addClass("collapse in");
						    	$('#ul-nav-third-' + leftMenuId).attr('aria-expanded', true);
						    	$('#ul-nav-third-' + leftMenuId).addClass("collapse in");
						    	$('#li-' + leftMenuId + ' .collapse').removeClass("collapse" );
						    	
#20160307
	consumer-web模块： 登陆页面增加 redirect 功能，解决登陆页面直接redirect到用户关注的页面功能；
							左侧菜单部分数据回显，移除cookie部分的依赖，防止左侧菜单由于更换浏览器造成回显失败；
								此部分处理逻辑在 AOP内部 ，LeftMenuDataAop.java 文件；
								
#20160308
	Dubbo-consumer模块：	增加按钮级别的权限控制
		解决方案：
			后台权限控制资源配置增加 按钮级别的权限控制部分，并且在分配角色权限时进行分配；
				注意 页面中的权限资源按钮部分的名称要跟页面的名称保持一致，这样，就可以针对按钮级别进行权限控制；
		Dubbo-persist模块： 数据库文件发生变化，进行更新；
		
#20160308
	Dubbo-consumer Dubbo-provider-read Dubbo-provider-write 三个模块：
		抽离 配置文件 xml 内部的 
			<dubbo:registry protocol="zookeeper" address="192.168.2.211:2181" />
	部分 到 Dubbo-api 模块中，减少重复代码；一个地方修改即可；
	
#2060310
	Dubbo-consumer 按钮级别的功能权限下，不能再存在按钮级别的功能权限；
	
	SpringMVC UTF-8 中文支持	如果SpringMVC 返回的是一个String字符串，这里会出现中文乱码的情况，可以参考如下代码：
	
		@RequestMapping(value = "/utf8Test", produces = "application/json; charset=utf-8")
		@ResponseBody
		public String utf8Test() {
			Map<String, String> map = new HashMap<String, String>();
			map.put("alexgaoyh", "中国");
			return JSONUtilss.toJSon(map);
		}
	
	如有有一个方法如上，通过浏览器访问，会发现如果存在中文情况的话，会出现中文乱码；
		此时需要注意  produces = "application/json; charset=utf-8"  这一段代码段落；
	
#20160321
	Dubbo-consumer 模块，一对多CRUD 功能增加多方delete处理
							
#20160322
	Dubbo-consumer 模块： 一对多CRUD 修复多方无值时数据查询失败的bug; 修复for循环时nullpointer异常；
	
#20160421
	Dubbo-RWSeperate-* 模块：
			将 sys_*  后台 RABC 相关的表结构都进行处理，移除 Example 相关的表结构，改为自定义的通用方法；
				本地扩展mybatis-generator 插件，自定义插件解决基础功能处理；
				后续有新的业务逻辑，则重新扩展此自定义插件；
				
#20160503
	<c:import>相比于<jsp:include> 提供了更加灵活的方式，功能性改进；
	<c:import> 允许您从其他的网络应用程序中指定内容，以及上下文，网络服务器，提供了更高的灵活性；
	一个静态的包括，总是比动态的快，意味着 <%@ include file="" %> 比后面两者都快  <jsp:include>  <c:import>.
	从技术上讲，如果你要求它的功能性，或是灵活性，性能的改进是最小的，则应该使用 <c:import>
	
#20160603
	移除Dubbo-consumer模块的 staticUrl 部分配置，静态资源改为绝对路径，同样能够解决动静分离的情况。
	
#20160626
	Sitemesh 的实现，修改 web.xml 内配置的 sitemesh 的默认filter 
		org.sitemesh.config.ConfigurableSiteMeshFilter
			com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.util.sitemesh.filter.ParamConfigurableSiteMeshFilter
				如果 url 请求中包含 decorator 参数，并且参数值为 true 的话，则默认不存在任何 装饰器效果