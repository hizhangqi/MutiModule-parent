<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<body>
	<link href="/css/plugins/ztree/zTreeStyle.css" rel="stylesheet">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>
					All form elements <small>With custom checbox and radion
						elements.</small>
				</h5>
			</div>
			<div class="ibox-content">
				<form method="post" class="form-horizontal" id="formHTML">
					<div class="form-group">
						<label class="col-sm-2 control-label">字典名称</label>
						<div class="col-sm-10">
							<input type="text" id="content" name="content" value="${dictDictionaryWithItemView.content }"
								class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">字典前缀</label>
						<div class="col-sm-10">
							<input type="text" id="perfixCode" name="perfixCode" value="${dictDictionaryWithItemView.perfixCode }"
								class="form-control">
						</div>
					</div>	
					<div class="form-group">
						<label class="col-sm-2 control-label">字典类型</label>
						<div class="col-sm-10">
							<input type="text" id="type" name="type" value="${dictDictionaryWithItemView.type }"
								class="form-control">
						</div>
					</div>			
					<div class="hr-line-dashed"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label">字典值</label>
						<div class="col-sm-10">
							<ul id="treeDemo" class="ztree"></ul>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<button class="btn btn-white" type="submit"
								onclick="javaScript:history.go(-1);return false;">返回</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!-- ztree -->
	<script src="/js/plugins/ztree/jquery.ztree.core-3.5.js"></script>
	<script src="/js/plugins/ztree/jquery.ztree.excheck.js"></script>
	<script src="/js/plugins/ztree/jquery.ztree.exedit.js"></script>
	<script type="text/javascript">
		var setting = {
			view: {
				selectedMulti: false
			},
			edit: {
				enable: true,
				showRemoveBtn: false,
				showRenameBtn: false
			},
			data: {
				keep: {
					parent:true,
					leaf:true
				},
				simpleData: {
					enable: true
				}
			}
		};
	
		var zNodes = ${zNodes};
	
		$( document ).ready(function() {
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
			disableForm('formHTML', true);
		});
		
		//禁用form表单中所有的input[文本框、复选框、单选框],select[下拉选],多行文本框[textarea]
		function disableForm(formId,isDisabled) {
		    
		    var attr="disable";
			if(!isDisabled){
			   attr="enable";
			}
			$("form[id='"+formId+"'] :text").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] :password").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] textarea").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] select").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] :radio").attr("disabled",isDisabled);
			$("form[id='"+formId+"'] :checkbox").attr("disabled",isDisabled);
		}
	</script>
</body>
</html>