<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE HTML>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>INSPINIA | Dashboard</title>
	<link href="/css/bootstrap.min.css" rel="stylesheet">
	<link href="/font-awesome/css/font-awesome.css" rel="stylesheet">
	
	<link href="/css/animate.css" rel="stylesheet">
	<link href="/css/style.css" rel="stylesheet">

	<!-- Mainly scripts -->
	<script src="/js/jquery-2.1.1.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<script src="/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	<script src="/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

</head>

<body>
	<jsp:include page="common.jsp"></jsp:include>
	<div id="wrapper">
		<jsp:include page="left.jsp"></jsp:include>

		<div id="page-wrapper" class="gray-bg dashbard-1">
			<jsp:include page="header.jsp"></jsp:include>

			<div id="manager_content" name="manager_content">
				<div class="row wrapper border-bottom white-bg page-heading">
					<div class="col-sm-4">
						<h2>This is main title</h2>
						<ol class="breadcrumb">
							<li><a href="index.html">This is</a></li>
							<li class="active"><strong>Breadcrumb</strong></li>
						</ol>
					</div>
					<div class="col-sm-8">
						<div class="title-action">
							<a href="" class="btn btn-primary">This is action area</a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="wrapper wrapper-content" id="manager_wrapperContent">
							<sitemesh:write property="body"/>
						</div>
					</div>
				</div>
				<jsp:include page="footer.jsp"></jsp:include>
			</div>


		</div>
	</div>

	<!-- Custom and plugin javascript -->
	<script src="/js/jquery.cookie.js"></script>
	<script src="/js/json/json2.js"></script>
	<script src="/js/inspinia.js"></script>
	<script src="/js/plugins/pace/pace.min.js"></script>

</body>
</html>
