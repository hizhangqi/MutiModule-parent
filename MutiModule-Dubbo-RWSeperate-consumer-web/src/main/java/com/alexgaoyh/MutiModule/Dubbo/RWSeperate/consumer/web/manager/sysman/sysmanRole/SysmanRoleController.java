package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.sysman.sysmanRole;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.MutiModule.common.exception.MyLogicException;
import com.MutiModule.common.exception.MyNumberFormatException;
import com.MutiModule.common.twitter.IDGenerator.instance.IdWorkerInstance;
import com.MutiModule.common.utils.DateUtils;
import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.utils.StringUtilss;
import com.MutiModule.common.vo.TreeNode;
import com.MutiModule.common.vo.ZTreeNodes;
import com.MutiModule.common.vo.enums.DeleteFlagEnum;
import com.MutiModule.common.vo.enums.MenuResourceTypeEnum;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read.ISysmanResourceReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read.ISysmanRoleReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.write.ISysmanRoleWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRoleResourceRel.read.ISysmanRoleResourceRelReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRole.SysmanRole;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel.SysmanRoleResourceRelKey;
import com.alibaba.dubbo.config.annotation.Reference;

/**
*
* dubbo 消费者
 * SysmanRole 模块 读接口
 * @author alexgaoyh
*
*/
@Controller
@RequestMapping(value="manager/sysman/sysmanRole")
public class SysmanRoleController extends BaseController<SysmanRole>{

	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read.ISysmanRoleReadService")
	private ISysmanRoleReadService readService;
	
	@Reference(group="writeService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.write.ISysmanRoleWriteService")
	private ISysmanRoleWriteService writeService;
	
	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRoleResourceRel.read.ISysmanRoleResourceRelReadService")
	private ISysmanRoleResourceRelReadService sysmanRoleResourceReadService;
	
	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read.ISysmanResourceReadService")
	private ISysmanResourceReadService sysmanResourceReadService;
	
	@Override
	public ModelAndView list(ModelAndView model, HttpServletRequest request) {
		
		super.list(model, request);
		
		List<TreeNode> treeNodeList = decorateCondition(request);

		model.addObject("treeNodeList", treeNodeList);
		
		model.addObject("menuResourceTypeEnumValues", MenuResourceTypeEnum.values());
		
		String id = request.getParameter("id");
		model.addObject("id", id);
		
		return model;
		
	}
	
	/**
	 * 封装参数部分
	 * @param request
	 * @return
	 */
	protected List<TreeNode> decorateCondition(HttpServletRequest request) {
		
		List<SysmanRole> _rootList = readService.selectSysmanRoleListByNullParentId();
		
		List<TreeNode> treeNodeList = new ArrayList<TreeNode>();
		
		for(SysmanRole _node : _rootList) {
			List<TreeNode> pageView = readService.selectTreeNodeBySysmanRoleId(_node.getId());
			treeNodeList.addAll(pageView);
		}
		
		return treeNodeList;
	}
	
	@Override
	public ModelAndView add(ModelAndView model, HttpServletRequest request) {
		
		super.add(model, request);
		
		List<ZTreeNodes> list = readService.selectAllResourceZTreeNode();
		model.addObject("zTreeListJSON", JSONUtilss.toJSon(list));
		
		model.addObject("menuResourceTypeEnumValues", MenuResourceTypeEnum.values());
		return model;
		
	}
	
	/**
	 * 保存
	 * 
	 * @return
	 * @throws IOException
	 */
	@Override
	public ModelAndView doSave(ModelAndView model, HttpServletRequest request, SysmanRole entity, RedirectAttributesModelMap modelMap) throws Exception  {
		super.doSave(model, request, entity, modelMap);
		
		beforeDoSave(request, entity);
		if(StringUtilss.isEmpty(entity.getParentId()) || entity.getParentId().equals("-1")) {
			entity.setParentId(null);
			entity.setLevels(0);
			entity.setParentIds(null);
		}
		writeService.insert(entity);
		afterDoSave(request, entity);
		
		modelMap.addAttribute("id", entity.getId());
		
		model.setViewName("redirect:list");

		return model;
	}

	/**
	 * 调用保存方法之前进行的方法调用
	 * @param request
	 * @param entity 对应实体信息
	 * @throws Exception
	 */
	protected void beforeDoSave(HttpServletRequest request, SysmanRole entity) throws Exception {
		String id = IdWorkerInstance.getIdStr();
		
		entity.setId(id);
		entity.setDeleteFlag(DeleteFlagEnum.NORMAL);
		entity.setCreateTime(DateUtils.getGMTBasicTime());
		
		//parentIds list集合
		List<String> _list = new ArrayList<String>();
		if(entity.getParentId() != null) {
			_list.add(entity.getParentId());
		}
		_list = readService.selectParentIdsByPrimaryKey(entity.getParentId(), _list);
		
		if(_list.size() > 3) {
			throw new MyNumberFormatException("后台资源管理-不能超过四层结构");
		} else {
			entity.setLevels(_list.size());
			entity.setParentIds(StringUtilss.getStringSeparatorByList(_list, ","));
		}
	}
	
	/**
	 * 电泳保存方法之后进行的方法调用
	 * @param request
	 * @param entity 对应实体信息
	 * @throws Exception
	 */
	protected void afterDoSave(HttpServletRequest request, SysmanRole entity) throws Exception {
		
	}
	
	@Override
	public ModelAndView doUpdate(ModelAndView model, HttpServletRequest request, SysmanRole entity, RedirectAttributesModelMap modelMap) throws Exception {
		super.doUpdate(model, request, entity, modelMap);
		
		beforeDoUpdate(request, entity);
		if(StringUtilss.isEmpty(entity.getParentId()) || entity.getParentId().equals("-1")) {
			entity.setParentId(null);
			entity.setLevels(0);
			entity.setParentIds(null);
		}
		writeService.updateByPrimaryKey(entity);
		afterDoUpdate(request, entity);

		modelMap.addAttribute("id", entity.getId());
		
		model.setViewName("redirect:list");

		return model;
	}
	
	/**
	 * 调用更新操作之前进行的操作
	 * @param request
	 * @param entity
	 * @throws Exception
	 */
	protected void beforeDoUpdate(HttpServletRequest request, SysmanRole entity) throws Exception {
		List<String> _list = new ArrayList<String>();
		_list.add(entity.getParentId());
		_list = readService.selectParentIdsByPrimaryKey(entity.getParentId(), _list);
		
		if(_list.size() > 3) {
			throw new MyNumberFormatException("后台角色管理-不能超过四层结构");
		} else {
			entity.setLevels(_list.size());
			entity.setParentIds(StringUtilss.getStringSeparatorByList(_list, ","));
		}
	}
	
	/**
	 * 调用更新操作之后进行的操作
	 * @param request
	 * @param entity
	 * @throws Exception
	 */
	protected void afterDoUpdate(HttpServletRequest request, SysmanRole entity) throws Exception {
		
	}
	
	@Override
	public ModelAndView show(@PathVariable("id") String id, ModelAndView model, HttpServletRequest request, RedirectAttributesModelMap modelMap) throws Exception  {
		
		super.show(id, model, request, modelMap);
		
		SysmanRole entity = readService.selectByPrimaryKey(id);
		model.addObject("entity", entity);
		
		List<ZTreeNodes> list = readService.selectAllResourceZTreeNode();
		model.addObject("zTreeListJSON", JSONUtilss.toJSon(list));
		
		model.addObject("menuResourceTypeEnumValues", MenuResourceTypeEnum.values());
		
		return model;
	}
	
	@Override
	public ModelAndView edit(@PathVariable("id") String id, ModelAndView model, HttpServletRequest request, RedirectAttributesModelMap modelMap) throws Exception  {
		
		super.edit(id, model, request, modelMap);
		
		SysmanRole entity = readService.selectByPrimaryKey(id);
		model.addObject("entity", entity);
		
		List<ZTreeNodes> list = readService.selectAllResourceZTreeNode();
		for(ZTreeNodes _zTree : list) {
			if(entity.getParentId() != null) {
				if(_zTree.getId() + "" == entity.getParentId()) {
					_zTree.setChecked(true);
				}
			}
		}
		model.addObject("zTreeListJSON", JSONUtilss.toJSon(list));
		
		model.addObject("menuResourceTypeEnumValues", MenuResourceTypeEnum.values());
		
		return model;
	}

}
