package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.MutiModule.common.utils.RandomUtilss;
import com.MutiModule.common.vo.TreeNode;

public class TreeNodeUtilss {

	public static List<TreeNode> generaterTreeNodeList2() {
		List<TreeNode> _list = new ArrayList<TreeNode>();

		TreeNode _tree1 = new TreeNode();
		_tree1.setId(1010L);
		_tree1.setText(1010 + "");
		_tree1.setLeaf(false);
		List<TreeNode> _temp = generaterTreeNodeList1();
		_temp.get(0).getChildren().get(0).setId(10L);
		_temp.get(0).getChildren().get(0).setText(10 + "");
		_tree1.setChildren(_temp);
		
		TreeNode _tree2 = new TreeNode();
		_tree2.setId(new Random().nextLong());
		_tree2.setText(RandomUtilss.randomInt(100, 1000) + "");
		_tree2.setLeaf(false);
		_tree2.setChildren(generaterTreeNodeList1());
		
		TreeNode _tree3 = new TreeNode();
		_tree3.setId(new Random().nextLong());
		_tree3.setText(RandomUtilss.randomInt(10, 100) + "");
		_tree3.setLeaf(false);
		_tree3.setChildren(generaterTreeNodeList0());
		
		TreeNode _tree4 = generaterTreeNode();
		
		_list.add(_tree1);
		_list.add(_tree2);
		_list.add(_tree3);
		_list.add(_tree4);
		
		return _list;
	}
	
	public static List<TreeNode> generaterTreeNodeList1() {
		List<TreeNode> _list = new ArrayList<TreeNode>();

		TreeNode _tree1 = new TreeNode();
		_tree1.setId(new Random().nextLong());
		_tree1.setText(RandomUtilss.randomInt(10, 100) + "");
		_tree1.setLeaf(false); 
		_tree1.setChildren(generaterTreeNodeList0());
		
		TreeNode _tree2 = new TreeNode();
		_tree2.setId(new Random().nextLong());
		_tree2.setText(RandomUtilss.randomInt(10, 100) + "");
		_tree2.setLeaf(false);
		_tree2.setChildren(generaterTreeNodeList0());
		
		_list.add(_tree1);
		_list.add(_tree2);
		
		return _list;
	}
	
	public static List<TreeNode> generaterTreeNodeList0() {
		List<TreeNode> _list = new ArrayList<TreeNode>();
		_list.add(generaterTreeNode());
		_list.add(generaterTreeNode());
		return _list;
	}
	
	public static TreeNode generaterTreeNode() {
		
		TreeNode tree = new TreeNode();
		tree.setId(new Random().nextLong());
		tree.setText(RandomUtilss.randomInt(0, 10) + "");
		tree.setLeaf(false);
		tree.setChildren(null);
		
		return tree;
	}
}
