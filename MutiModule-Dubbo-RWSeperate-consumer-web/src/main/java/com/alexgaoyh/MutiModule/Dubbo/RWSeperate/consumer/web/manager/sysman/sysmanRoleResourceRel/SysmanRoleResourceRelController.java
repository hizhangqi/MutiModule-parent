package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.sysman.sysmanRoleResourceRel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.ArrayUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.utils.StringUtilss;
import com.MutiModule.common.vo.ZTreeNodes;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read.ISysmanResourceReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read.ISysmanRoleReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRoleResourceRel.read.ISysmanRoleResourceRelReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRoleResourceRel.write.ISysmanRoleResourceRelWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanResource.SysmanResource;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanRoleResourceRel.SysmanRoleResourceRelKey;
import com.alibaba.dubbo.config.annotation.Reference;

/**
*
* dubbo 消费者
 * SysmanRoleResourceRel 模块 读接口
 * @author alexgaoyh
*
*/
@Controller
@RequestMapping(value="manager/sysman/sysmanRoleResourceRel")
public class SysmanRoleResourceRelController extends BaseController<SysmanRoleResourceRelKey>{

	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRoleResourceRel.read.ISysmanRoleResourceRelReadService")
	private ISysmanRoleResourceRelReadService readService;
	
	@Reference(group="writeService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRoleResourceRel.write.ISysmanRoleResourceRelWriteService")
	private ISysmanRoleResourceRelWriteService writeService;
	
	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanResource.read.ISysmanResourceReadService")
	private ISysmanResourceReadService sysmanResourceReadService;
	
	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanRole.read.ISysmanRoleReadService")
	private ISysmanRoleReadService sysmanRoleReadService;
	
	@RequestMapping(value = "/roleResourceRelList/{id}")
	public ModelAndView roleResourceRelList(@PathVariable("id") Long id, ModelAndView model, HttpServletRequest request) {

		RequestMapping rm = this.getClass().getAnnotation(RequestMapping.class);
		String moduleName = "";
		if (rm != null) {
			String[] values = rm.value();
			if (ArrayUtils.isNotEmpty(values)) {
				moduleName = values[0];
			}
		}
		if (moduleName.endsWith("/")) {
			moduleName = moduleName.substring(0, moduleName.length() - 1);
		}
		
		List<ZTreeNodes> zTreenNodesList = new ArrayList<ZTreeNodes>();
		List<SysmanResource> resourceList = sysmanResourceReadService.selectListByMap(null);
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("sysmanRoleId", id);
		List<SysmanRoleResourceRelKey> roleResourceRelKeyList = readService.selectListByMap(map);
		
		for(SysmanResource _resource : resourceList) {
			ZTreeNodes _zTreeNodes = new ZTreeNodes();
			_zTreeNodes.setId(Long.parseLong(_resource.getId()));
			if(StringUtilss.isNotEmpty(_resource.getParentId())) {
				_zTreeNodes.setpId(Long.parseLong(_resource.getParentId()));
			} else {
				_zTreeNodes.setpId(0L);
			}
			_zTreeNodes.setName(_resource.getName());
			_zTreeNodes.setOpen(true);
			
			boolean _checked = false;
			for(SysmanRoleResourceRelKey _relKey : roleResourceRelKeyList) {
				if((_resource.getId() + "").equals(_relKey.getSysmanResourceId())) {
					_checked = true;
				}
			}
			_zTreeNodes.setChecked(_checked);
			
			zTreenNodesList.add(_zTreeNodes);
		}
		model.addObject("zTreenNodesList", JSONUtilss.toJSon(zTreenNodesList));
		model.addObject("sysmanRoleId", id);
		
		String currentPageStr = request.getParameter("currentPage");
		if(StringUtilss.isEmpty(currentPageStr)) {
			currentPageStr = "1";
		}
		
		String recordPerPageStr = request.getParameter("recordPerPage");
		if(StringUtilss.isEmpty(recordPerPageStr)) {
			recordPerPageStr = "10";
		}
		model.addObject("currentPage", currentPageStr);
		model.addObject("recordPerPage", recordPerPageStr);
		
		model.addObject("moduleName", moduleName);
		model.setViewName(moduleName + "/roleResourceRelList");
		
		return model;
	}
	
	/**
	 * 保存
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/saveResourceRel")
	public ModelAndView saveResourceRel(ModelAndView model, HttpServletRequest request, RedirectAttributesModelMap modelMap) throws Exception  {
		String sysmanResourceIds = request.getParameter("sysmanResourceIds");
		String sysmanRoleId = request.getParameter("sysmanRoleId");
		
        writeService.refreshSysmanRoleResourceRel(sysmanRoleId, sysmanResourceIds);
        
        modelMap.addAttribute("id", sysmanRoleId);
		
		model.setViewName("redirect:/manager/sysman/sysmanRole/list");

		return model;
	}

}
