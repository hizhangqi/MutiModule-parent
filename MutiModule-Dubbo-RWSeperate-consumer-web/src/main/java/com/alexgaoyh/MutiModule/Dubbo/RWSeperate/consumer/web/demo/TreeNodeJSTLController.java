package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.vo.TreeNode;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.demo.util.TreeNodeUtilss;

@Controller
@RequestMapping(value="demo/treeNodeJSTLEach")
public class TreeNodeJSTLController {

	@RequestMapping(value = "/index")
	public ModelAndView index(ModelAndView model, HttpServletRequest request) {
		
		List<TreeNode> treeList = TreeNodeUtilss.generaterTreeNodeList2();
		System.out.println(JSONUtilss.toJSon(treeList));
		
		model.addObject("treeList", treeList);
		
		model.setViewName("demo/treeNodeJSTLEach/index");
		return model;
	}
	
}
