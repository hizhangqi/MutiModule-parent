package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.manager.demo.oneWithMany.student;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import com.MutiModule.common.myPage.MyPageView;
import com.MutiModule.common.twitter.IDGenerator.instance.IdWorkerInstance;
import com.MutiModule.common.utils.DateUtils;
import com.MutiModule.common.utils.StringUtilss;
import com.MutiModule.common.vo.enums.DeleteFlagEnum;
import com.MutiModule.common.vo.mybatis.pagination.Page;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.course.read.IOneWithManyCourseReadService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.course.write.IOneWithManyCourseWriteService;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.consumer.web.base.BaseController;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course.OneWithManyCourse;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.student.OneWithManyStudent;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.view.CourseStudentView;
import com.alibaba.dubbo.config.annotation.Reference;

/**
*
* dubbo 消费者
 * OneWithManyStudent 模块 读接口
 * @author alexgaoyh
*
*/
@Controller
@RequestMapping(value="manager/demo/oneWithMany/courseStudent")
public class OneWithManyCourseStudentController extends BaseController<OneWithManyCourse>{

	@Reference(group="readService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.course.read.IOneWithManyCourseReadService")
	private IOneWithManyCourseReadService readService;
	
	@Reference(group="writeService", interfaceName="com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.demo.oneWithMany.course.write.IOneWithManyCourseWriteService")
	private IOneWithManyCourseWriteService writeService;
	
	@Override
	public ModelAndView list(ModelAndView model, HttpServletRequest request) {
		
		super.list(model, request);
		
		MyPageView<CourseStudentView> pageView = decorateCondition(request);

		model.addObject("pageView", pageView);
		
		return model;
		
	}
	
	/**
	 * 封装参数部分
	 * @param request
	 * @return
	 */
	protected MyPageView<CourseStudentView> decorateCondition(HttpServletRequest request) {
		
		String currentPageStr = request.getParameter("currentPage");
		if(StringUtilss.isEmpty(currentPageStr)) {
			currentPageStr = "1";
		}
		
		String recordPerPageStr = request.getParameter("recordPerPage");
		if(StringUtilss.isEmpty(recordPerPageStr)) {
			recordPerPageStr = "10";
		}

		int beginInt = Integer.parseInt(currentPageStr) >=1 ? Integer.parseInt(currentPageStr) : 1;
		
		Page page = new Page((beginInt - 1)*Integer.parseInt(recordPerPageStr), Integer.parseInt(recordPerPageStr));
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("page", page);
		
		//TODO 搜索条件添加
		String listSearchNameStr = request.getParameter("listSearchName");
		if(StringUtilss.isNotEmpty(listSearchNameStr)) {
			map.put("listSearchName", "%" + listSearchNameStr + "%");
		}
		
		MyPageView<CourseStudentView> pageView = readService.generateMyPageViewVOWithStudent(map);
		
		return pageView;
	}
	
	/**
	 * 保存
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/doSaveView")
	public ModelAndView doSave(ModelAndView model, HttpServletRequest request, CourseStudentView entity)
			throws Exception {
		System.out.println(entity);
		entity.setCreateTime(DateUtils.getGMTBasicTime());
		entity.setDeleteFlag(DeleteFlagEnum.NORMAL);
		entity.setId(IdWorkerInstance.getIdStr());
		List<OneWithManyStudent> studentList = entity.getStudentList();
		if(studentList != null) {
			for(OneWithManyStudent _student : studentList) {
				_student.setId(IdWorkerInstance.getIdStr());
				_student.setCreateTime(DateUtils.getGMTBasicTime());
				_student.setDeleteFlag(DeleteFlagEnum.NORMAL);
			}
		}
		writeService.insertCourseStudentView(entity);
		
		model.setViewName("redirect:list");
		
		return model;
	}
	
	@Override
	public ModelAndView show(@PathVariable("id") String id, ModelAndView model, HttpServletRequest request, RedirectAttributesModelMap modelMap) throws Exception {
		super.show(id, model, request, modelMap);

		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("id", id);
		List<CourseStudentView> list = readService.selectCourseWithStudentByExample(map);
		
		model.addObject("entity", list.get(0));
		
		return model;
	}
	
	@Override
	public ModelAndView edit(@PathVariable("id") String id, ModelAndView model, HttpServletRequest request, RedirectAttributesModelMap modelMap) throws Exception {
		
		super.edit(id, model, request, modelMap);

		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("id", id);
		List<CourseStudentView> list = readService.selectCourseWithStudentByExample(map);
		
		if(list != null && list.size() > 0) {
			model.addObject("entity", list.get(0));
		}
		
		return model;
	}
	
	/**
	 * 保存
	 * 
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value="/doUpdateView")
	public ModelAndView doUpdate(ModelAndView model, HttpServletRequest request, CourseStudentView entity, RedirectAttributesModelMap modelMap)
			throws Exception {
		writeService.updateCourseStudentView(entity);
		String currentPageStr = request.getParameter("currentPage");
		if(StringUtilss.isEmpty(currentPageStr)) {
			currentPageStr = "1";
		}
		
		String recordPerPageStr = request.getParameter("recordPerPage");
		if(StringUtilss.isEmpty(recordPerPageStr)) {
			recordPerPageStr = "10";
		}
		modelMap.addAttribute("currentPage", currentPageStr);
		modelMap.addAttribute("recordPerPage", recordPerPageStr);
		
		model.setViewName("redirect:list");
		
		return model;
	}

}
