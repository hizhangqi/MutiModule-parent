package com.MutiModule.common.mybatis.annotation.data.authority;

/**
 * 注解当前登陆用户的主键ID的注解类
 * @author alexgaoyh
 *
 */
public @interface DataAuthorityCurrSysUseLoginAnnotation {

	String value() default "";
}
