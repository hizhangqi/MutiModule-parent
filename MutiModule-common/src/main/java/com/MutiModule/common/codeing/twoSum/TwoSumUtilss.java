/**
 * File : TwoSumUtilss.java <br/>
 * Author : lenovo <br/>
 * Version : 1.1 <br/>
 * Date : 2016年12月23日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.codeing.twoSum <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.codeing.twoSum;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName : TwoSumUtilss <br/>
 * Function : 一个数组，一个求和值，算出来哪两个数字相加能够算出来这个值. <br/>
 * Reason : TODO ADD REASON. <br/>
 * Date : 2016年12月23日 下午4:47:32 <br/>
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

public class TwoSumUtilss {
	
	public static void main(String[] args) {
		Integer[] arrays = new Integer[]{1,2,3,4,5,6,7,8,9};
		Integer sum = 11;
		twoSum(arrays, sum);
	}

	/**
	 * 功能：tempMap中，key 是 值，value 是数组的顺序号， 
	 * 	如果tempMap 中不包含对应的值信息，将值 和 顺序号分别作为 key 和 value 放到 tempMap 中<br/>
	 *
	 * @author alexgaoyh
	 * @version 2017年1月2日 上午11:45:35 <br/>
	 */
	public static String twoSum(Integer[] arrays, Integer sum) {
		Map<Integer, Integer> tempMap = new HashMap<Integer, Integer>();
		for (int i = 0; i < arrays.length; i ++) {
			if(tempMap.containsKey(sum - arrays[i])) {
				System.out.println(arrays[i]);
				System.out.println(sum - arrays[i]);
				return null;
			} else {
				tempMap.put(arrays[i], i);
			}
		}
		return null;
	}
}
