package com.MutiModule.common.fileld.validator.referee;

import com.MutiModule.common.fileld.validator.AbstractReferee;
import com.MutiModule.common.fileld.validator.Rule.Chinese;
import com.MutiModule.common.fileld.validator.State;

/**
 * 中文校验器
 * 
 * 检查值是否为中文
 * 
 */
public class ChineseReferee extends AbstractReferee<Chinese> {

	@Override
	public State check(Object data) {
		return regexMatch("^[\\u0391-\\uFFE5\\s\\t\\n\\x0B\\f\\r]+$", data, getMessageRuleFirst("string.chinese", "The value is not chinese"));
	}

	@Override
	public State check(Object data, String serviceLine) {
		String annotationServiceLine = this.rule.serviceLine();
		if(annotationServiceLine != null && !annotationServiceLine.equals("")) {
			if(annotationServiceLine.contains(serviceLine)) {
				return check(data);
			} 
		} 
		return new State(true, "");
	}

}
