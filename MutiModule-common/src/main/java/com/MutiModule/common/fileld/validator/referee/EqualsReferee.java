package com.MutiModule.common.fileld.validator.referee;

import com.MutiModule.common.fileld.validator.AbstractCompareReferee;
import com.MutiModule.common.fileld.validator.Rule.Equals;
import com.MutiModule.common.fileld.validator.State;

/**
 * 相等
 */
public class EqualsReferee extends AbstractCompareReferee<Equals> {
	
	@Override
	public State check(Object data) {
		Object target = getFieldValue(rule.value());
		if (target == data)
			return simpleSuccess();
		if (data != null && data.equals(target))
			return simpleSuccess();
		return failure(getMessageRuleFirst("object.equals", "The data is not equals with target field."));
	}
	
	
	@Override
	public State check(Object data, String serviceLine) {
		String annotationServiceLine = this.rule.serviceLine();
		if(annotationServiceLine != null && !annotationServiceLine.equals("")) {
			if(annotationServiceLine.contains(serviceLine)) {
				return check(data);
			} 
		} 
		return new State(true, "");
	}
}
