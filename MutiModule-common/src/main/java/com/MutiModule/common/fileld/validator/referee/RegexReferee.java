package com.MutiModule.common.fileld.validator.referee;

import com.MutiModule.common.fileld.validator.AbstractReferee;
import com.MutiModule.common.fileld.validator.Rule.Regex;
import com.MutiModule.common.fileld.validator.State;

public class RegexReferee extends AbstractReferee<Regex> {

	@Override
	public State check(Object data) {
		return regexMatch(rule.value(), data, String.format(getMessageRuleFirst("object.equals", "The data named %s is not match regex."), rule.name()));
	}
	
	
	@Override
	public State check(Object data, String serviceLine) {
		String annotationServiceLine = this.rule.serviceLine();
		if(annotationServiceLine != null && !annotationServiceLine.equals("")) {
			if(annotationServiceLine.contains(serviceLine)) {
				return check(data);
			} 
		} 
		return new State(true, "");
	}

}
