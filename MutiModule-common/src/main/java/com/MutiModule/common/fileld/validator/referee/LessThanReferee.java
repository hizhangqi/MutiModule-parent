package com.MutiModule.common.fileld.validator.referee;

import java.math.BigDecimal;

import com.MutiModule.common.fileld.validator.AbstractCompareReferee;
import com.MutiModule.common.fileld.validator.Rule.LessThan;
import com.MutiModule.common.fileld.validator.State;

/**
 * 对数字进行小于等于比较
 * 
 */
public class LessThanReferee extends AbstractCompareReferee<LessThan> {

	@Override
	public State check(Object data) {
		// 进行数字转换
		if (!(data instanceof Number)) {
			return failure(getMessageRuleFirst("number.lessThan", String.format("The field is not type of Number.", fieldName)));
		}
		Object target = getFieldValue(rule.value());
		if (!(target instanceof Number)) {
			return failure(getMessageRuleFirst("number.lessThan", String.format("The target is not type of Number.", fieldName)));
		}

		BigDecimal number = new BigDecimal(data + ""), targetNumber = new BigDecimal(target + "");

		if (number.doubleValue() < targetNumber.doubleValue())
			return simpleSuccess();
		else if (rule.equalable() && number.doubleValue() == targetNumber.doubleValue())
			return simpleSuccess();
		else
			return failure(getMessageRuleFirst("number.lessThan", "The data is not less than target data"));
	}
	
	@Override
	public State check(Object data, String serviceLine) {
		String annotationServiceLine = this.rule.serviceLine();
		if(annotationServiceLine != null && !annotationServiceLine.equals("")) {
			if(annotationServiceLine.contains(serviceLine)) {
				return check(data);
			} 
		} 
		return new State(true, "");
	}
}
