package com.MutiModule.common.utils;

import java.io.InputStream;
import java.util.Properties;

/**
 * Dubbo-RWSeparate-consumer-web.properties 配置文件读取
 * @author lenovo
 *
 */
public class DubboRWSepConWebPropUtilss {
	
	private static String adminCookie;

	static{
		
        loads();
        
    }
	
    synchronized static public void loads(){
    	
        if(adminCookie == null){
            InputStream is = DubboRWSepConWebPropUtilss.class.getResourceAsStream("/Dubbo-RWSeparate-consumer-web.properties");
            Properties dproperties = new Properties();
            try {
            	dproperties.load(is);
                        
            	adminCookie = dproperties.getProperty("adminCookie").toString();
            	
            }
            catch (Exception e) {
                System.err.println("不能读取属性文件. " + "请确保 idWorker.properties 在CLASSPATH指定的路径中");
            }
        }
        
    }

	public static String getAdminCookie() {
		return adminCookie;
	}
}
