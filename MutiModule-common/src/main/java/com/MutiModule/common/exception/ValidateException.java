/**
 * File : ValidateException.java <br/>
 * Author : alexgaoyh <br/>
 * Version : 1.1 <br/>
 * Date : 2017年6月22日 <br/>
 * Modify : <br/>
 * Package Name : com.MutiModule.common.exception <br/>
 * Project Name : MutiModule-common <br/>
 * Description : <br/>
 * 
 */

package com.MutiModule.common.exception;

import com.MutiModule.common.utils.StringUtilss;

/**
 * ClassName : ValidateException <br/>
 * Function : 校验异常类. <br/>
 * Reason : 校验异常类. <br/>
 * Date : 2017年6月22日 下午4:59:12 <br/>
 * 
 * @author : alexgaoyh <br/>
 * @version : 1.1 <br/>
 * @since : JDK 1.6 <br/>
 * @see
 */

public class ValidateException extends RuntimeException{

	public ValidateException(Throwable e) {
		super(e.getMessage(), e);
	}
	
	public ValidateException(String message) {
		super(message);
	}
	
	public ValidateException(String messageTemplate, Object... params) {
		super(StringUtilss.format(messageTemplate, params));
	}
	
	public ValidateException(String message, Throwable throwable) {
		super(message, throwable);
	}
	
	public ValidateException(Throwable throwable, String messageTemplate, Object... params) {
		super(StringUtilss.format(messageTemplate, params), throwable);
	}
}
