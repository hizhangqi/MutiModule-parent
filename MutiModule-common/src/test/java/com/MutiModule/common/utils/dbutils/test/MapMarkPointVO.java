package com.MutiModule.common.utils.dbutils.test;

public class MapMarkPointVO {

	private String name;
	
	private Integer value;
	
	private Double[] geoCoord;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getValue() {
		return value;
	}

	public void setValue(Integer value) {
		this.value = value;
	}

	public Double[] getGeoCoord() {
		return geoCoord;
	}

	public void setGeoCoord(Double[] geoCoord) {
		this.geoCoord = geoCoord;
	}
	
}
