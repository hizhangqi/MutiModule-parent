package com.MutiModule.common.utils.poi.ss.formula.functions;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * excel 操作，写入excel值，并且将公式部分计算出来的值输出
 * excel 内的A1 单元格里面包含公式  =B1+C1+D1
 * 测试代码 是在  B1 C1 D1 内部将值写入，
 * 并且输出 A1 内的单元格计算后的值
 * @author alexgaoyh
 *
 */
public class TestFormula {

	public static void main(String[] args) throws FileNotFoundException {
		write();
		read();
	}
	
	public static void write() {
		try {
			
			POIFSFileSystem fs;
			fs = new POIFSFileSystem(new FileInputStream("e://test.xls"));
			HSSFWorkbook wb = new HSSFWorkbook(fs);
			HSSFSheet sheet = wb.getSheet("Sheet1");
			HSSFRow row = sheet.getRow((short) 0);
			
			HSSFCell cell = row.getCell(1);
			if (cell == null) {
				cell = row.createCell(1);
			}
			cell.setCellValue(5);

			cell = row.getCell(2);
			if (cell == null) {
				cell = row.createCell(2);
			}
			cell.setCellValue(5);

			cell = row.getCell(3);
			if (cell == null) {
				cell = row.createCell(3);
			}
			cell.setCellValue(40);

			FileOutputStream fileOut = new FileOutputStream("e://test.xls");

			wb.write(fileOut);
			fileOut.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void read() {
		try {
			FileInputStream fis = new FileInputStream("E:/test.xls");  
			Workbook wb = new HSSFWorkbook(fis); 
			Sheet sheet = wb.getSheetAt(0);  
			FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();  
			  
			// suppose your formula is in B3  
			CellReference cellReference = new CellReference("A1");   
			Row row = sheet.getRow(cellReference.getRow());  
			Cell cell = row.getCell(cellReference.getCol());   
			  
			CellValue cellValue = evaluator.evaluate(cell);  
			  
			switch (cellValue.getCellType()) {  
			    case Cell.CELL_TYPE_BOOLEAN:  
			        System.out.println(cellValue.getBooleanValue());  
			        break;  
			    case Cell.CELL_TYPE_NUMERIC:  
			        System.out.println(cellValue.getNumberValue());  
			        break;  
			    case Cell.CELL_TYPE_STRING:  
			        System.out.println(cellValue.getStringValue());  
			        break;  
			    case Cell.CELL_TYPE_BLANK:  
			        break;  
			    case Cell.CELL_TYPE_ERROR:  
			        break;  
			  
			    // CELL_TYPE_FORMULA will never happen  
			    case Cell.CELL_TYPE_FORMULA:   
			        break;  
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
