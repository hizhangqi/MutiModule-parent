package com.MutiModule.common.utils.jdbc;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.junit.Test;

import com.MutiModule.common.utils.HttpClientUtilss;
import com.MutiModule.common.utils.JSONUtilss;
import com.MutiModule.common.utils.JdbcUtil;
import com.MutiModule.common.utils.jdbc.vo.DistanceVO;
import com.MutiModule.common.utils.jdbc.vo.GeoLocationVO;
import com.MutiModule.common.utils.jdbc.vo.StatusVO;

public class JdbcUtilTest {

	//@Test
	public void testJdbc() {
		try {
			Connection conn = JdbcUtil.getConnection();
			Statement stat =  conn.createStatement();
			ResultSet rs = stat.executeQuery(" select t.nameCN from base_location t where t.`level` = 2 and t.nameCN != '苗栗县' and t.nameCN != '南投县' and t.nameCN != '其他' ; ");
			
			List<GeoLocationVO> list = new ArrayList<GeoLocationVO>();
			
			while(rs.next()){
				String nameCN = rs.getString("nameCN");
				String positionjson = HttpClientUtilss.get("http://api.map.baidu.com/geocoder/v2/?address=" + nameCN.trim()  +"&output=json&ak=2b11331c434b89a7e12ad132a1c376a2");
				StatusVO status = JSONUtilss.readValue(positionjson, StatusVO.class);
				
				System.out.println(positionjson);
				GeoLocationVO vo = new GeoLocationVO(nameCN, status.getResult().getLocation().getLng(), status.getResult().getLocation().getLat());
				list.add(vo);
			}
			
			// 创建excel
	        HSSFWorkbook wb = new HSSFWorkbook();
	        // 创建sheet
	        HSSFSheet sheet = wb.createSheet("区域距离");
	        
	        int rownum = 0;
	        
	        System.out.println("--------------------------------------------------------------------");
			
			for(int i = 0; i < list.size(); i++) {
				for(int j = i+1; j < list.size(); j++) {
					GeoLocationVO geoLocationVO_i = list.get(i);
					GeoLocationVO geoLocationVO_j = list.get(j);
					
					String positionjson = HttpClientUtilss.get("http://api.map.baidu.com/telematics/v3/distance?waypoints=" + geoLocationVO_i.getLat() + "," + geoLocationVO_i.getLng() + ";" + 
							geoLocationVO_j.getLat() + "," + geoLocationVO_j.getLng() + "&ak=NXO0D9UthHAdoT5c3S5lwvGH&output=json");
					
					System.out.println(positionjson);
					DistanceVO distance = JSONUtilss.readValue(positionjson, DistanceVO.class);
					
					if(distance == null || distance.getResults() == null || distance.getResults().isEmpty()) {
						continue;
					}
					// 创建一行
			        HSSFRow rowTitle = sheet.createRow(rownum++);
			        
			        // 在行上创建1列
			        HSSFCell cell0 = rowTitle.createCell(0);
			        cell0.setCellValue(geoLocationVO_i.getName());
			        
			        HSSFCell cell1 = rowTitle.createCell(1);
			        cell1.setCellValue(geoLocationVO_j.getName());
			        
			        HSSFCell cell2 = rowTitle.createCell(2);
			        cell2.setCellValue(distance.getResults().get(0).toString());
			        
			        System.out.println(geoLocationVO_i.getName() + ":" + geoLocationVO_j.getName() + ":" + distance.getResults().get(0).toString());
					
				}
			}
			
			FileOutputStream fout;
			try {
				fout = new FileOutputStream("d://a.xls");
				try {
					wb.write(fout);
					fout.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
				
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void testJ1() {
		String ps = HttpClientUtilss.get("http://api.map.baidu.com/telematics/v3/distance?waypoints=118.77147503233,32.054128923368;116.3521416286,39.965780080447&ak=YusEjGt3ycwTw8VrzPqbXo8u&output=json");

		DistanceVO distance = JSONUtilss.readValue(ps, DistanceVO.class);
		
		System.out.println(distance.getResults().get(0));

	}
	
	//@Test
	public void testExcel() {
		// 创建excel
        HSSFWorkbook wb = new HSSFWorkbook();
        // 创建sheet
        HSSFSheet sheet = wb.createSheet("运单数据");
        // 创建一行
        HSSFRow rowTitle = sheet.createRow(0);
        // 在行上创建1列
        HSSFCell cellTitle = rowTitle.createCell(0);
        cellTitle.setCellValue("运单号");
		FileOutputStream fout;
		try {
			fout = new FileOutputStream("d://a.xls");
			try {
				wb.write(fout);
				fout.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
}
