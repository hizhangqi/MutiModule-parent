package com.MutiModule.common.utils.xml;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "item")
public class Item {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 销售订单编号
	 */
	private String MANDT;

	/**
	 * 记录的创建日期 
	 */
	private String POSNR;

	/**
	 * 销售凭证类型 
	 */
	private String MATNR;

	/**
	 * 送达方 
	 */
	private String KWMENG;

	/**
	 * 售达方 
	 */
	private String VRKME1;

	/**
	 * 销售组织 
	 */
	private String LFIMG;

	/**
	 * 产品组 
	 */
	private String REKMG;

	/**
	 * 客户组 
	 */
	private String VRKME2;

	/**
	 * 住宅号及街道
	 */
	private String KBETR;

	/**
	 * 销售和分销凭证的项目号
	 */
	private String NETPR;

	/**
	 * 物料号 
	 */
	private String NETWR;

	/**
	 * 物料组 
	 */
	private String WAERK;

	public String getMANDT() {
		return MANDT;
	}

	public void setMANDT(String mANDT) {
		MANDT = mANDT;
	}

	public String getPOSNR() {
		return POSNR;
	}

	public void setPOSNR(String pOSNR) {
		POSNR = pOSNR;
	}

	public String getMATNR() {
		return MATNR;
	}

	public void setMATNR(String mATNR) {
		MATNR = mATNR;
	}

	public String getKWMENG() {
		return KWMENG;
	}

	public void setKWMENG(String kWMENG) {
		KWMENG = kWMENG;
	}

	public String getVRKME1() {
		return VRKME1;
	}

	public void setVRKME1(String vRKME1) {
		VRKME1 = vRKME1;
	}

	public String getLFIMG() {
		return LFIMG;
	}

	public void setLFIMG(String lFIMG) {
		LFIMG = lFIMG;
	}

	public String getREKMG() {
		return REKMG;
	}

	public void setREKMG(String rEKMG) {
		REKMG = rEKMG;
	}

	public String getVRKME2() {
		return VRKME2;
	}

	public void setVRKME2(String vRKME2) {
		VRKME2 = vRKME2;
	}

	public String getKBETR() {
		return KBETR;
	}

	public void setKBETR(String kBETR) {
		KBETR = kBETR;
	}

	public String getNETPR() {
		return NETPR;
	}

	public void setNETPR(String nETPR) {
		NETPR = nETPR;
	}

	public String getNETWR() {
		return NETWR;
	}

	public void setNETWR(String nETWR) {
		NETWR = nETWR;
	}

	public String getWAERK() {
		return WAERK;
	}

	public void setWAERK(String wAERK) {
		WAERK = wAERK;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
}
