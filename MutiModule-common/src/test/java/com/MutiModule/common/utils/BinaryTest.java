package com.MutiModule.common.utils;

import org.junit.Before;
import org.junit.Test;

/**
 * 使用二进制数据的格式，用单个数据表示他所包含的多个数据信息
 * 如此例中，column是一个数据，转换为二进制数据之后，每一位都表示一个用户角色，
 * 每一位表示的用户角色使用 role1 role2 role3 来代替，其中这些角色必须是2的倍数，这样，才能够使用二进制数据的某一位表示角色
 * @author alexgaoyh
 *
 */
public class BinaryTest {
	
	//标示用户角色集合的一个数字部分
	private Integer column;
	
	private Integer role1;
	
	private Integer role2;
	
	private Integer role3;

	//@Before
	public void init() {
		column = 3;
		role1 = 1;
		role2 = 2;
		role3 = 4;
	}
	
	//@Test
	public void isRole1() {
		boolean returnBool =  ((column&role1) > 0) ? true : false;
		System.out.println("isRole1 = " + returnBool);
	}
	
	//@Test
	public void isRole2() {
		boolean returnBool =  ((column&role2) > 0) ? true : false;
		System.out.println("isRole2 = " + returnBool);
	}
	
	//@Test
	public void isRole3() {
		boolean returnBool =  ((column&role3) > 0) ? true : false;
		System.out.println("isRole3 = " + returnBool);
	}
}
