package com.MutiModule.common.utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.junit.Test;

import com.MutiModule.common.utils.DateUtils;

public class DateUtilssTest {

	// @Test
	public void getTodayIntevalDaysTest() {
		// 相差多少天
		Long inteval = DateUtils.getTodayIntevalDays("2015-09-16");
		System.out.println(inteval);
	}

	// @Test
	public void timeZoneTest() {
		try {
			String str = "20151111125959:+0800";
			SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss:z", Locale.US);
			Date date = df.parse(str);
			System.out.println(date.toGMTString());
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	// @Test
	public void getGMTBasicTimeTest() {
		// GMT 标准时间
		System.out.println(DateUtils.getGMTBasicTime());
	}
}