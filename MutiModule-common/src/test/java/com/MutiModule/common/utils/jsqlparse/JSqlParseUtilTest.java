package com.MutiModule.common.utils.jsqlparse;

import java.util.List;

import org.junit.Test;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.parser.CCJSqlParserUtil;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.util.TablesNamesFinder;

public class JSqlParseUtilTest {

	// @Test
	public void sqlParseTest() {
		try {
			Select select = (Select)CCJSqlParserUtil.parse("SELECT * FROM tab1");
			TablesNamesFinder tablesNamesFinder = new TablesNamesFinder();
			List<String> tableList = tablesNamesFinder.getTableList(select);
			for (String string : tableList) {
				System.out.println(string);
			}
		} catch (JSQLParserException e) {
			e.printStackTrace();
		}
	}
}
