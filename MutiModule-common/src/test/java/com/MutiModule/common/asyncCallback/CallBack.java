package com.MutiModule.common.asyncCallback;

/**
 * 定义一个回调接口
 * @author alexgaoyh
 *
 */
public interface CallBack {

	/**
	 * 在解决问题之后，响应的函数，也就是回调函数
	 * @param result	回调函数传递的result返回值
	 */
	public void solve(String result);  
}
