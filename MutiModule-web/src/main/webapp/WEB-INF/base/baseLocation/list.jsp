<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>省市区管理</title>

	<link rel="stylesheet" type="text/css"
		href="${pageContext.request.contextPath}/css/zTree/zTreeStyle.css">
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery/jquery-v1.11.3.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/jquery/form/jQuery.form.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/js/zTree/jquery.ztree.core-3.5.js"></script>
	<script type="text/javascript"
	src="${pageContext.request.contextPath}/js/form-extend/form-extend.js"></script>
	
	<SCRIPT type="text/javascript">
		var context_ = '${context_}';
	
		<!--
		var setting = {
			async: {
				enable: true,
				contentType:"application/x-www-form-urlencoded",
                url: context_ + "/base/baseLocation/getAnscLocationjson",
                autoParam:["id"],
                dataType:"json"
			},
			data : {
				simpleData : {
					enable : true,
	                idKey: "id",//使用简单必须标明的的节点对应字段  
	                pIdKey: "parentId",//使用简单必须标明的的父节点对应字段 
				}
		    },
			callback: {
				beforeClick: beforeClick,
				onClick: onClick
			}
		};
		
		function beforeClick(treeId, treeNode, clickFlag) {
			return true;
		}
		
		function onClick(event, treeId, treeNode, clickFlag) {
			console.log(treeNode.id);
			loadForm(treeNode.id);
			return true;
		}
		
		function loadForm(treeNodeId) {
			$.post(context_ + "/base/baseLocation/selectByPrimaryKey",
				{
					"id": treeNodeId
				},
				function(data) {
					FormExtend.formLoadData(data, 'baseLocationForm');
				}
			);
		}
		
		function updateBaseLocation() {
			$("#submit").attr('disabled',"true");
			
			$.ajax({
				type: "POST",
				url:context_ + "/base/baseLocation/updateBaseLocation",
				data:$('#baseLocationForm').serialize(),// 要提交的表单 
				success: function(data) {
					var obj = eval("("+data+")");
					alert(obj.msg);
				}
			});
			
			FormExtend.resetForm('baseLocationForm');
			$('#submit').removeAttr("disabled");
		}
		
		$(document).ready(function(){
			$.fn.zTree.init($("#treeDemo"), setting);
		});
		//-->
	</SCRIPT>
	
</head>
<body>
	<div style="float: left; width: 40%; ">
		<ul id="treeDemo" class="ztree"></ul>
	</div>
	<div style="float: right; width: 40%; ">
	
		<form id="baseLocationForm">
			主键：<input type="text" name="id" id="id" /> <br></br>
			中文名：<input type="text" name="nameCN" id="nameCN" /> <br></br>
			英文名：<input type="text" name="nameEN" id="nameEn" /> <br></br>
			短名称：<input type="text" name="longName" id="longName" /> <br></br>
			长名称：<input type="text" name="shortName" id="shortName" /> <br></br>
			父节点Id：<input type="text" name="parentId" id="parentId" /> <br></br>
			<input type="button" value="提交" id="submit" onclick="updateBaseLocation();">
		</form>
		
	</div>
</body>
</html>