<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE HTML>
<html lang="en">

	<head>
	</head>

	<body>
		<div class="w_wrap pb_70">

			<div class="u_head text_left u_head_01">
				<span class="f24">springmvc Form表单提交 重复提交测试 token部分</span>
				<a href="javaScript:history.go(-1);" class="fl">
					<i class="bg_sprite icon_back"></i>
				</a>
				<a href="${pageContext.request.contextPath}/">
					<i class="bg_sprite icon_homeSmall fr"></i>
				</a>
			</div>

			<div class="infoDet c_333">
				<form id="info_form_id" action="${pageContext.request.contextPath}/${moduleName }/save" method="POST">
					<input type="hidden" name="token" value="${token }" />
					<input type="text" name="name" value="${name  }" />
					<input type="submit"></input>
				</form>
			</div>

		</div>
	</body>

</html>