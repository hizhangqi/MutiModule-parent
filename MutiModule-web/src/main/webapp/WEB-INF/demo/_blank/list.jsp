<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Manager</title>

	<jsp:include page="../../common/adminCommon.jsp"></jsp:include>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui-extend/easyui_dataGrid_blank_extend.js?v=" + Math.ceil(new Date()/3600000)></script>
	
</head>
<body class="easyui-layout" >
	<div  data-options="region:'north'" class="search" id ="search" style="height: 50px;">
		名称： <input id="searchName" name="searchName" />
		<a id="search-btn" href="#" class="easyui-linkbutton" data-options="iconCls:'icon-search'"   >search</a>
	</div>
	
	<div data-options="region:'center',split:false">
		<table id="dg-1" class="easyui-datagrid" title="列表" style="width: 700px; height: 300px"
			data-options="toolbar:'#toolbar-1',checkOnSelect:true,selectOnCheck:true,fit:true,rownumbers:true,fitColumns:true,url:'${pageContext.request.contextPath}/${moduleName}/getData',method:'get',pagination:true">
			<thead>
				<tr>
					<th data-options="field:'ck',checkbox:true"></th>
					<th data-options="field:'id',width:80">编码</th>
					<th data-options="field:'name',width:80,formatter:function(value,row,index){
						var url = '${pageContext.request.contextPath}/demo/_blank/edit/'+row.id;
						return '<a href=\''+url+'\'>'+value+'</a>' ;
					}">用户名</th>
					<th data-options="field:'createTime',width:80,formatter:function(value){var date=parseDate(value);return date;}">创建时间</th>
					<th data-options="field:'deleteFlag',width:80,formatter:function(value){
							<c:forEach var="data" items="${deleteFlagEnum}" varStatus="status">
								if('${data }' == value) {
									return '${data.name }'
								}
							</c:forEach>
					}">删除状态</th>
				</tr>
			</thead>
		</table>
		
		<div id="toolbar-1">
			<a href="#" class="easyui-linkbutton add" iconCls="icon-add" plain="true">新增</a> 
			<!-- <a href="#" class="easyui-linkbutton edit" iconCls="icon-edit" plain="true">修改</a>  -->
			<a href="#" class="easyui-linkbutton remove" iconCls="icon-remove" plain="true">删除</a>
		</div>
		
	</div>
	
	<script type="text/javascript">
		
		$( function() {
			
			var dg1 = new DataGridBlankEasyui(context_, 1 , templateUrl, undefined);
			
			dg1.init();
			
			$("#search-btn").on('click',function (){
				$("#dg-1").datagrid('load',{
					searchName : $("#searchName").val()
				});
			});
			
		});
	</script>
	
</body>
</html>