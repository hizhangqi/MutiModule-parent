package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary;

import com.MutiModule.common.mybatis.annotation.*;
import com.MutiModule.common.mybatis.base.BaseEntity;
import java.io.Serializable;

@MyBatisTableAnnotation(name = "sys_dict_dictionary", namespace = "com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionaryMapper", remarks = "数据字典-表头信息 ", aliasName = "sys_dict_dictionary sys_dict_dictionary")
public class DictDictionary extends BaseEntity implements Serializable {
	/**
	 * 字典名称,所属表字段为sys_dict_dictionary.CONTENT
	 */
	@MyBatisColumnAnnotation(name = "CONTENT", value = "sys_dict_dictionary_CONTENT", chineseNote = "字典名称", tableAlias = "sys_dict_dictionary")
	private String content;

	/**
	 * 类型,所属表字段为sys_dict_dictionary.TYPE
	 */
	@MyBatisColumnAnnotation(name = "TYPE", value = "sys_dict_dictionary_TYPE", chineseNote = "类型", tableAlias = "sys_dict_dictionary")
	private String type;

	/**
	 * 编码前缀,所属表字段为sys_dict_dictionary.PERFIX_CODE
	 */
	@MyBatisColumnAnnotation(name = "PERFIX_CODE", value = "sys_dict_dictionary_PERFIX_CODE", chineseNote = "编码前缀", tableAlias = "sys_dict_dictionary")
	private String perfixCode;

	private static final long serialVersionUID = 1L;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPerfixCode() {
		return perfixCode;
	}

	public void setPerfixCode(String perfixCode) {
		this.perfixCode = perfixCode;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(getClass().getSimpleName());
		sb.append(" [");
		sb.append("Hash = ").append(hashCode());
		sb.append(", content=").append(content);
		sb.append(", type=").append(type);
		sb.append(", perfixCode=").append(perfixCode);
		sb.append("]");
		return sb.toString();
	}
}