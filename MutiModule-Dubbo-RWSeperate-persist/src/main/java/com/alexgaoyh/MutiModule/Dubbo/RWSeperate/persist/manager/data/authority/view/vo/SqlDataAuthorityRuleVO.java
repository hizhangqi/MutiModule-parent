package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.vo;

import java.io.Serializable;
import java.util.List;

/**
 * 类的绝对路径和类的注释描述 并包含这个类下的所有属性值集合
 * 形如  类路径	com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.dict.dictionary.DictDictionary
 * 的描述 '数据字典-表头信息 '
 * 并且下面包含的属性集合  content type  perfixCode
 * @author alexgaoyh
 *
 */
public class SqlDataAuthorityRuleVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2785839723213424683L;
	
	public SqlDataAuthorityRuleVO(String classPath, String remarks) {
		this.classPath = classPath;
		this.remarks = remarks;
	}

	/**
	 * 类路径部分
	 */
	private String classPath;
	
	/**
	 * 类路径对应的实体类的注释部分
	 */
	private String remarks;
	
	/**
	 * 类下包含的属性集合的部分
	 */
	private List<SqlDataAuthorityModelVO> items;

	public String getClassPath() {
		return classPath;
	}

	public void setClassPath(String classPath) {
		this.classPath = classPath;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public List<SqlDataAuthorityModelVO> getItems() {
		return items;
	}

	public void setItems(List<SqlDataAuthorityModelVO> items) {
		this.items = items;
	}
	
}
