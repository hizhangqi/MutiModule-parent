package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.groupRuleRel;

import java.io.Serializable;

import com.MutiModule.common.mybatis.annotation.MyBatisColumnAnnotation;

public class SqlDataAuthorityOperationGroupRuleRelKey implements Serializable {
    /**
     *  数据权限组ID-主键外键,所属表字段为sys_data_authority_group_rule_rel.SYS_DATA_AUTHORITY_GROUP_ID
     */
    @MyBatisColumnAnnotation(name = "SYS_DATA_AUTHORITY_GROUP_ID", value = "sys_data_authority_group_rule_rel_SYS_DATA_AUTHORITY_GROUP_ID", chineseNote = "数据权限组ID-主键外键", tableAlias = "sys_data_authority_group_rule_rel")
    private String sysDataAuthorityGroupId;

    /**
     *  数据权限规则ID-主键外键,所属表字段为sys_data_authority_group_rule_rel.SYS_DATA_AUTHORITY_RULE_ID
     */
    @MyBatisColumnAnnotation(name = "SYS_DATA_AUTHORITY_RULE_ID", value = "sys_data_authority_group_rule_rel_SYS_DATA_AUTHORITY_RULE_ID", chineseNote = "数据权限规则ID-主键外键", tableAlias = "sys_data_authority_group_rule_rel")
    private String sysDataAuthorityRuleId;

    private static final long serialVersionUID = 1L;

    public String getSysDataAuthorityGroupId() {
        return sysDataAuthorityGroupId;
    }

    public void setSysDataAuthorityGroupId(String sysDataAuthorityGroupId) {
        this.sysDataAuthorityGroupId = sysDataAuthorityGroupId;
    }

    public String getSysDataAuthorityRuleId() {
        return sysDataAuthorityRuleId;
    }

    public void setSysDataAuthorityRuleId(String sysDataAuthorityRuleId) {
        this.sysDataAuthorityRuleId = sysDataAuthorityRuleId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", sysDataAuthorityGroupId=").append(sysDataAuthorityGroupId);
        sb.append(", sysDataAuthorityRuleId=").append(sysDataAuthorityRuleId);
        sb.append("]");
        return sb.toString();
    }
}