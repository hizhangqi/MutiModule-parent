package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.util.interceptor.constants.vo;

/**
 * 	SQL 操作符
 * @author alexgaoyh
 *
 */
public class SqlModelVO {
	
	/**
	 * 表结构别名
	 */
	private String aliasTabelName;

	/**
	 * key
	 */
	private String prop;
	
	/**
	 * 操作符
	 */
	private String operation;
	
	/**
	 * value
	 */
	private String values;
	
	
	public SqlModelVO(String aliasTabelName, String prop, String operation, String values) {
		this.aliasTabelName = aliasTabelName;
		this.prop = prop;
		this.operation = operation;
		this.values = values;
	}


	public String getAliasTabelName() {
		return aliasTabelName;
	}


	public void setAliasTabelName(String aliasTabelName) {
		this.aliasTabelName = aliasTabelName;
	}


	public String getProp() {
		return prop;
	}


	public void setProp(String prop) {
		this.prop = prop;
	}


	public String getOperation() {
		return operation;
	}


	public void setOperation(String operation) {
		this.operation = operation;
	}


	public String getValues() {
		return values;
	}


	public void setValues(String values) {
		this.values = values;
	}
	
	
	
}
