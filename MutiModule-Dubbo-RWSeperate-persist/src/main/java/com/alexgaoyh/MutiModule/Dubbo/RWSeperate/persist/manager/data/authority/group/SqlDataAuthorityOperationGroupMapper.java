package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group;

import java.util.List;
import java.util.Map;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationGroupWithRuleView;

public interface SqlDataAuthorityOperationGroupMapper {
	int deleteByPrimaryKey(String id);

	int selectCountByMap(Map<Object, Object> map);

	List<SqlDataAuthorityOperationGroup> selectListByMap(Map<Object, Object> map);

	int insert(SqlDataAuthorityOperationGroup record);

	int insertSelective(SqlDataAuthorityOperationGroup record);

	SqlDataAuthorityOperationGroup selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(SqlDataAuthorityOperationGroup record);

	int updateByPrimaryKey(SqlDataAuthorityOperationGroup record);
	
	// alexgaoyh
	
	/**
	 * 根据map 集合属性，查询出来数据权限组集合信息（包含数据权限规则部分）
	 * @param map	map集合，与类中属性相符合
	 * @return
	 */
	List<SqlDataAuthorityOperationGroupWithRuleView> selectListWithRuleByMap(Map<Object, Object> map);
	
	/**
	 * 根据主键ID，查询出 数据权限组 的实体信息（包含组下规则部分）
	 * @param id	主键ID
	 * @return	数据权限组 信息（包含数据权限规则部分）
	 */
	SqlDataAuthorityOperationGroupWithRuleView selectGroupWithRuleViewById(String id);
}