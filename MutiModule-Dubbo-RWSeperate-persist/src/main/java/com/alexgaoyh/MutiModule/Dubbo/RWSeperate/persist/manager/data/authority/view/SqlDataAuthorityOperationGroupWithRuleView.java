package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view;

import java.util.List;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.group.SqlDataAuthorityOperationGroup;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule.SqlDataAuthorityOperationRule;

/**
 * 数据权限集合，数据权限组，包含组下的规则ID集合
 * @author alexgaoyh
 *
 */
public class SqlDataAuthorityOperationGroupWithRuleView extends SqlDataAuthorityOperationGroup{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<SqlDataAuthorityOperationRule> items;

	public List<SqlDataAuthorityOperationRule> getItems() {
		return items;
	}

	public void setItems(List<SqlDataAuthorityOperationRule> items) {
		this.items = items;
	}
	
	
}
