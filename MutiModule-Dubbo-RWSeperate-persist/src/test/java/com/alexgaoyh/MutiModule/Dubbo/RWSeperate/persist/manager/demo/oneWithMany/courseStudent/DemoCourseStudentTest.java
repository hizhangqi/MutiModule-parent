package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.courseStudent;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.vo.mybatis.pagination.Page;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.course.OneWithManyCourseMapper;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.demo.oneWithMany.view.CourseStudentView;

public class DemoCourseStudentTest {

	private OneWithManyCourseMapper mapper;

	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (OneWithManyCourseMapper) ctx.getBean( "oneWithManyCourseMapper" );
        
    }
	
	//@Test
	public void selectCourseWithStudentByExampleTest() {
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("id", 11104965584430080L);
		
		List<CourseStudentView> list = mapper.selectCourseWithStudentByExample(null);
		System.out.println("selectCourseWithStudentByExampleTest = " + list.size());
	}
	
	//@Test
	public void selectCourseWithStudentListByExampleTest() {
		
		String currentPageStr = "1";
		
		String recordPerPageStr = "10";

		int beginInt = Integer.parseInt(currentPageStr) >=1 ? Integer.parseInt(currentPageStr) : 1;
		
		Page page = new Page((beginInt - 1)*Integer.parseInt(recordPerPageStr), Integer.parseInt(recordPerPageStr));
		
		Map<Object, Object> map = new HashMap<Object, Object>();
		map.put("page", page);
		
		List<CourseStudentView> list = mapper.selectCourseWithStudentByExample(map);
		System.out.println("selectCourseWithStudentListByExampleTest = " + list.size());
	}
}
