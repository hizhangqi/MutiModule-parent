package com.MutiModule.lucene.entity;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.InvalidTokenOffsetsException;
import org.apache.lucene.store.FSDirectory;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.lucene.HighlighterParam;
import com.MutiModule.lucene.LuceneUtils;
import com.MutiModule.lucene.Page;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocation;
import com.alexgaoyh.MutiModule.persist.base.location.BaseLocationMapper;

/**
 * lucene 操作，从数据库中获取到List<Entity> 集合信息，并保存到索引文件中
 * 并且可对索引文件进行分页高亮信息查询
 * @author lenovo
 *
 */
public class LuceneEntityTest {
	
	private BaseLocationMapper mapper;
	
	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (BaseLocationMapper) ctx.getBean( "baseLocationMapper" );
        
    }

	//@Test
	public void testAll() throws IOException, ParseException, InvalidTokenOffsetsException {
		createIndex();
		testPagewithHighlighter();
		
	}
	
	/**
	 * 生成索引文件
	 * @throws IOException
	 */
	public void createIndex() throws IOException {
		
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
		// 配置索引
		IndexWriterConfig writerConfig = new IndexWriterConfig(LuceneUtils.analyzer);
        
		IndexWriter iWriter = LuceneUtils.getIndexWrtier(directory, writerConfig);
        
		List<BaseLocation> list = mapper.selectByExample(null);
		
		for(BaseLocation bl : list) {
			Document doc = new Document();
			
			doc.add(new Field("id", bl.getId().toString(), TextField.TYPE_STORED));
			doc.add(new Field("createTime", bl.getCreateTime().toString(), TextField.TYPE_STORED));
			doc.add(new Field("nameCN", bl.getNameCN(), TextField.TYPE_STORED));
			
			iWriter.addDocument(doc);
		}
		
		LuceneUtils.closeIndexWriter(iWriter);
		
	}
	
	/**
	 * 高亮显示的搜索结果，可以完成相关分页操作，
	 * 分页相关参数设置，详见 new Page<Document>(2, 2); 的分页参数定义
	 * @throws ParseException
	 * @throws IOException
	 * @throws InvalidTokenOffsetsException
	 */
	public void testPagewithHighlighter() throws ParseException, IOException, InvalidTokenOffsetsException {
		
		FSDirectory directory = LuceneUtils.openFSDirectory("D://TEST");
		// 配置索引
		IndexWriterConfig writerConfig = new IndexWriterConfig(LuceneUtils.analyzer);
        
		IndexWriter iWriter = LuceneUtils.getIndexWrtier(directory, writerConfig);
        
		IndexReader iReader = LuceneUtils.getIndexReader(directory);
		
		IndexSearcher iSearcher = LuceneUtils.getIndexSearcher(iReader);
		
		// 使用同样的方式对多field进行搜索
        String[] multiFields = { "id", "nameCN" };
        MultiFieldQueryParser parser = new MultiFieldQueryParser(multiFields, LuceneUtils.analyzer);
        // 设定具体的搜索词
        Query query = parser.parse("许昌");
        
        HighlighterParam hParam = new HighlighterParam(true, "nameCN", null, null, 100);
        
        //TODO 此处的限制，为设定当前分页相关，第几页，此页有多少条数据
        Page<Document> page = new Page<Document>(2, 2);
        
        LuceneUtils.pageQuery(iSearcher, directory, query, page, hParam, writerConfig, iWriter);
        for(Document document : page.getItems()) {
        	System.out.println(document.get("nameCN") + "_____" + document.get("id"));
        }
        
        LuceneUtils.closeAll(iReader, iWriter);
		LuceneUtils.closeDirectory(directory);
        
	}
}
