<%@ page language="java" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String url = request.getParameter("goto");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>系统登录界面</title>
<script type="text/javascript" src="<%=path%>/js/jquery-v1.11.3.js"></script>

<style type="text/css">
body {
	font: normal 11px auto "Trebuchet MS", Verdana, Arial, Helvetica,
		sans-serif;
	color: #4f6b72;
}

table {
	margin-top: 10%;
	margin-left: 30%;
	border: 1px solid #CCCCFF;
}

table td {
	border: 0px solid #CCCCFF;
	font: bold 12px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
}

table input {
	width: 200px;
}

.leftTd {
	text-align: right;
	width: 35%;
}

.centerTd {
	text-align: center;
	font: bold 18px "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
}

.rightTd {
	text-align: left;
	width: 65%;
}

#btn_ok {
	width: 50px;
}

#btn_clear {
	width: 50px;
}

a {
	margin: 30px;
}
</style>
<script type="text/javascript">

    $(function(){
         $("#btn_clear").bind("click",function(){
              btn_clear();
        });
         $("#btn_ok").bind("click",function(){
              btn_ok();
        });
    });

	function btn_ok(){
	    $("#form").attr("action","login.page");
	    $("#form").submit();
	}

	function btn_clear() {
		$(":input").not("input[type=button]").each(function() {
			$(this).val("");
		});
	}

</script>

</head>



<body>

	<form id="form" action="login.jsp" method="post">
		<table>
			<tbody>
				<tr>
					<td colspan="99" class="centerTd">用户信息登录</td>
				</tr>
				<tr>
					<td class="leftTd"><label> 用户名 </label></td>
					<td class="rightTd"><input type="text" name="username" /></td>
				</tr>
				<tr>
					<td class="leftTd"><label> 密码 </label></td>
					<td class="rightTd"><input type="password" name="userpassword" /></td>
				</tr>
				<tr>
					<td class="leftTd"><input type="button" id="btn_ok" value="登录" /></td>
					<td class="rightTd"><input type="button" id="btn_clear"
						value="重置" />
					</td>
				</tr>
			</tbody>
		</table>

		<input name="goto" type="hidden" value=<%=url%> />
	</form>
</body>
</html>