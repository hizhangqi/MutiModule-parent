package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.rule.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.rule.SqlDataAuthorityOperationRule;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.view.SqlDataAuthorityOperationView;

/**
 * SqlDataAuthorityOperationRule 模块 读接口
 * @author alexgaoyh
 *
 */
public interface ISqlDataAuthorityOperationRuleReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<SqlDataAuthorityOperationRule> selectListByMap(Map<Object, Object> map);

	SqlDataAuthorityOperationRule selectByPrimaryKey(String id);
    
    /**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<SqlDataAuthorityOperationRule> generateMyPageViewVO(Map<Object, Object> map);
    
	// alexgaoyh
	
	/**
	 * 根据map 属性，查询此属性下的所有包含 配置子模型的 数据权限属性规则
	 * @param map
	 * @return
	 */
	List<SqlDataAuthorityOperationView> selectListWithOperationModelByMap(Map<Object, Object> map);
	
	/**
	 * 根据主键ID查询实体信息，包含 明细结构
	 * @param id	主键ID
	 * @return	实体信息，包含 明细结构 数据
	 */
	SqlDataAuthorityOperationView selectEntityWithOperationModelById(String id);
	
	/**
     * 获取分页实体信息部分
     * @param map	参数传递，封装部分过滤参数
     * @return
     */
    MyPageView<SqlDataAuthorityOperationView> generateMyPageViewVOWithModel(Map<Object, Object> map);
    
}
