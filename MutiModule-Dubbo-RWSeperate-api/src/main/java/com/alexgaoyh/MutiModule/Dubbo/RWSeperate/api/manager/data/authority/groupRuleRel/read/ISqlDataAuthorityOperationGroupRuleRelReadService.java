package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.data.authority.groupRuleRel.read;

import java.util.List;
import java.util.Map;

import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.data.authority.groupRuleRel.SqlDataAuthorityOperationGroupRuleRelKey;

/**
 * SqlDataAuthorityOperationGroupRuleRel 模块 读接口
 * @author alexgaoyh
 *
 */
public interface ISqlDataAuthorityOperationGroupRuleRelReadService {

	int selectCountByMap(Map<Object, Object> map);

	List<SqlDataAuthorityOperationGroupRuleRelKey> selectListByMap(Map<Object, Object> map);

}
