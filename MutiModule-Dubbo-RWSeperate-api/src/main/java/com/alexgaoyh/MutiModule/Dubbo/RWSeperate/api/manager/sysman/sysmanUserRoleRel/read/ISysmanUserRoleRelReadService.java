package com.alexgaoyh.MutiModule.Dubbo.RWSeperate.api.manager.sysman.sysmanUserRoleRel.read;

import java.util.List;
import java.util.Map;

import com.MutiModule.common.myPage.MyPageView;
import com.alexgaoyh.MutiModule.Dubbo.RWSeperate.persist.manager.sysman.sysmanUserRoleRel.SysmanUserRoleRelKey;

/**
 * SysmanUserRoleRel 模块 读接口
 * @author alexgaoyh
 *
 */
public interface ISysmanUserRoleRelReadService {

    int selectCountByMap(Map<Object, Object> map);

    List<SysmanUserRoleRelKey> selectListByMap(Map<Object, Object> map);

    /**
     * 获取分页实体信息部分
     * @param exampleWithOutPage	参数传递，封装部分过滤参数
     * @param page	实体分页类部分
     * @return
     */
    MyPageView<SysmanUserRoleRelKey> generateMyPageViewVO(Map<Object, Object> map);
    
}
