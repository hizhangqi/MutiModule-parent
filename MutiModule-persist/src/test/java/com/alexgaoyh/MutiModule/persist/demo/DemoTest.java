package com.alexgaoyh.MutiModule.persist.demo;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.MutiModule.common.vo.enums.DeleteFlagEnum;

/**
 * 注意进行数据格式转换的过程中，如果之前是非json串的话，会报错
 * org.codehaus.jackson.JsonParseException
 * 需要注意版本迭代的过程中遇到的此类问题
 * @author lenovo
 *
 */
public class DemoTest {

	private DemoMapper mapper;

	//@Before
    public void prepare() throws Exception {
    	
        ApplicationContext ctx = new ClassPathXmlApplicationContext( "mybatis-spring-config.xml" );
        
        mapper = (DemoMapper) ctx.getBean( "demoMapper" );
        
    }
	
	//@Test
	public void testSelect() {
		try {
			Demo entity = mapper.selectByPrimaryKey(45);
			if(entity != null) {
				System.out.println(entity);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//@Test
	public void selectByC() {
		DemoExample example = new DemoExample();
		example.or().andDeleteFlagEqualTo(DeleteFlagEnum.DELETE);
		List<Demo> list = mapper.selectByExample(example);
		for(Demo demo : list) {
			System.out.println(demo.getName());
		}
	}
}
